/*****************************************
 * Autor: Darío Andrés Peña Quintero
 * Empresa: UTP - MINTIC
 * Descripción: Ejemplo de composición
 * Año: 2021
 * Gitlab: ---
 ****************************************/
public class Universidad{
    /*************
     * Atributos
     ************/
    private String nombre;
    private String nit;

    /*************
     * Constructor
     *************/
    public Universidad(String nombre, String nit){
        this.nombre = nombre;
        this.nit = nit;
    }
    /*
    public Universidad(){
        this.nombre = "";
        this.nit = "";
    }
    */
    

    /***********
     * Métodos
     **********/
    public void crear_facultad(String nombre){
        Facultad objFacultad = new Facultad("UTP", this);
        objFacultad.crear_carreras("Ingeniería ambiental");
    }

}